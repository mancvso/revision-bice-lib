package cl.bice.clients.payin;

import cl.bice.services.payments.ReactorPaymentServiceGrpc;
//import lombok.extern.apachecommons.CommonsLog;
import io.grpc.Channel;

// @CommonsLog
public class PaymentsServiceClient {
    public static ReactorPaymentServiceGrpc.ReactorPaymentServiceStub newClientFor(Channel channel) {
        return ReactorPaymentServiceGrpc.newReactorStub(channel);
    }
}