package cl.bice.clients.alerts;
//import lombok.extern.apachecommons.CommonsLog;
import cl.bice.services.alerts.ReactorAlertServiceGrpc;
import io.grpc.Channel;

// @CommonsLog
public class AlertsServiceClient {
    public static ReactorAlertServiceGrpc.ReactorAlertServiceStub newClientFor(Channel channel) {
        return ReactorAlertServiceGrpc.newReactorStub(channel);
    }
}